package com.mayaswell.randomplayer;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * Created by dak on 8/31/2016.
 *   component for interacting with the widget
 * handles communication with the widget and buttons, and launches a service to play sound files
 * the widget gives two buttons, one to start the service, and one to stop it
 */
public class WidgetProvider extends AppWidgetProvider {
	protected static final String clickPlay = "com.mayaswell.randomplayer.clickButtonPlay";
	protected static final String clickPause = "com.mayaswell.randomplayer.clickButtonPause";

	private RandomPlayer.LocalBinder playerBinder = null;
	private String currentTitle = null;
	private String currentArtist = null;

	/**
	 * anonymous listener from the RandomPlayer service
	 */
	private RandomPlayer.Listener playerListener = new RandomPlayer.Listener() {
		@Override
		public void trackChanged(String title, String artist) {
			Log.d("widget", "track changed to "+title+", "+artist);
			currentTitle = title;
			currentArtist = artist;
		}

		@Override
		public void onError(String msg) {
			Log.d("widget", "error "+msg);
		}
	};

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
		RemoteViews widgetView = new RemoteViews(context.getPackageName(), R.layout.randomplayer_widget);
		Log.d("WidgtProvider", "onUpdate() "+appWidgetIds.length+" widgets "+context.getPackageName());
		for (int widgetId: appWidgetIds) {
			Log.d("WidgtProvider", "onUpdate() widget "+widgetId);
			widgetView.setOnClickPendingIntent(R.id.play_button, makePendingIntent(context, widgetId, clickPlay));
			widgetView.setOnClickPendingIntent(R.id.pause_button, makePendingIntent(context, widgetId, clickPause));
		}
		ComponentName myWidget = new ComponentName(context, WidgetProvider.class);
		appWidgetManager.updateAppWidget(myWidget, widgetView);

	}

	/**
	 * make an appropriate intent for the button actions
	 * @param context
	 * @param widgetId
	 * @param action
	 * @return
	 */
	private PendingIntent makePendingIntent(Context context, int widgetId, String action) {
		Intent intent = new Intent(context, WidgetProvider.class);
		intent.setAction(action);
		intent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, widgetId);
		return PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	}


	@Override
	public void onAppWidgetOptionsChanged(Context context, AppWidgetManager appWidgetManager, int appWidgetId, Bundle newOptions) {
		Log.d("WidgtProvider", "onAppWidgetOptionsChanged()");
		super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		Log.d("WidgetProvider", "onDeleted()");
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onEnabled(Context context) {
		Log.d("WidgetProvider", "onEnabled()");
		super.onEnabled(context);
	}

	@Override
	public void onDisabled(Context context) {
		Log.d("WidgtProvider", "onDisabled()");
		super.onDisabled(context);
	}

	/**
	 * handle the incoming intents from the buttons
	 * @param context
	 * @param intent
	 */
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.d("WidgetProvider", "onReceive()");
		super.onReceive(context, intent);
		String a = intent.getAction();
		if (a == null) {
			Log.d("WidgetProvider", "on receive default action");
		} else if (a.equals(clickPlay)) {
			startPlayer(context);
		} else if (a.equals(clickPause)) {
			stopPlayer(context);
		}
	}

	/**
	 * start the RandomPlayer service
	 * @param context
	 */
	protected void startPlayer(Context context) {
		Intent intent = new Intent(context, RandomPlayer.class);
		intent.setAction(RandomPlayer.ACTION_PLAY);
		context.startService(intent);
		playerBinder = (RandomPlayer.LocalBinder) peekService(context, intent);
		if (playerBinder != null) {
			playerBinder.addListener(playerListener);
		}
	}

	/**
	 * stop the RandomPlayer service
	 * @param context
	 */
	protected void stopPlayer(Context context) {
		Intent intent = new Intent(context, RandomPlayer.class);
		intent.setAction(RandomPlayer.ACTION_PAUSE);
		try {
			context.stopService(intent);
			if (playerBinder != null) {
				playerBinder.removeListener(playerListener);
			}
		} catch (IllegalArgumentException e) { // we're already stopped
			e.printStackTrace();
			Log.d("WidgtProvider", e.getMessage());
		}
	}

}
