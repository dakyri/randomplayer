package com.mayaswell.randomplayer;

import android.app.IntentService;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Binder;
import android.os.IBinder;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.app.NotificationCompat;
import android.util.Log;

import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by dak on 8/31/2016.
 *   the media playing service. Runs an mp3 player asynchronously, and generatets a notification with
 * every track change
 */
public class RandomPlayer extends Service {
	public static final String ACTION_PLAY = "com.mayaswell.randomplayer.actionPlay";
	public static final String ACTION_PAUSE = "com.mayaswell.randomplayer.actionPause";

	private MediaPlayer mediaPlayer = null;
	private String currentArtist = null;
	private String currentTitle = null;
	private final int notificationId = 1; // constant notification Id

	@Override
	public void onCreate() {
		super.onCreate();
		Log.d("RandomPlayer", "onCreate()");
	}

	/**
	 * our main call back to kick things into action. builds the media player if it hasn't already, and sets
	 * it up with a new track either way
	 * @param intent
	 * @param flags
	 * @param startId
	 * @return
	 */
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("RandomPlayer", "onStartCommand()");
		currentArtist = null;
		currentTitle = null;
		String a = intent.getAction();
		if (a == null || a.equals(ACTION_PLAY)) {
			if (mediaPlayer == null) {
				buildMediaPlayer();
			} else {
				mediaPlayer.reset();
			}
			mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
			setRandomDataSource();
			mediaPlayer.prepareAsync();

		} else if (a.equals(ACTION_PAUSE)) {
			if (mediaPlayer != null) {
				mediaPlayer.reset();
			}
			clearNotification();
			stopSelf();
		}
		return START_STICKY;
	}

	/**
	 * lifecycle callback when we have been bound
	 * @param intent
	 * @return
	 */
	@Nullable
	@Override
	public IBinder onBind(Intent intent) {
		Log.d("RandomPlayer", "onBind()");
		if (binder == null) {
			binder = new LocalBinder();
		}
		return binder;
	}

	/**
	 * our service has been unbound
	 * @param intent
	 * @return
	 */
	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}

	/**
	 * our existing service is re-bound without deletion
	 * @param intent
	 */
	@Override
	public void onRebind(Intent intent) {
		super.onRebind(intent);
	}

	/**
	 * end of lifecycle
	 */
	@Override
	public void onDestroy() {
		super.onDestroy();
		mediaPlayer.reset();
		clearNotification();
		Log.d("RandomPlayer", "onDestroy()");
	}

	/**
	 * builds the media player, sets up parameters and callbacks
	 */
	private void buildMediaPlayer() {
		Log.d("RandomPlayer", "buildMediaPlayer()");
		mediaPlayer = new MediaPlayer();
		mediaPlayer.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
			@Override
			public void onPrepared(MediaPlayer mp) {
				mp.start();
			}
		});
		mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

			@Override
			public void onCompletion(MediaPlayer mp) {
				mp.reset();
				setRandomDataSource();
				mediaPlayer.prepareAsync();
			}
		});
		mediaPlayer.setOnErrorListener(new MediaPlayer.OnErrorListener() {
			@Override
			public boolean onError(MediaPlayer mp, int what, int extra) {
				return false;
			}
		});
	}

	/**
	 * sets the data for the media player. selects an mp3 at random, using the content resolver for media files
	 * notifies any listeners of whatever is happening
	 * @return
	 */
	private boolean setRandomDataSource() {
		if (mediaPlayer == null ) {
			return false;
		}
		ContentResolver contentResolver = getContentResolver();
		Uri searchUri = android.provider.MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
		Cursor cursor = contentResolver.query(searchUri, null, null, null, null);
		Uri dataUri = null;
		currentArtist = null;
		currentTitle = null;
		if (cursor == null) {
			notifyError("Query failed");
			return false;
		} else if (!cursor.moveToFirst()) {
			notifyError("No media");
			cursor.close();
			return false;
		} else {
			int total = cursor.getCount();
			int titleColumn = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
			int artistColumn = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
			int dataColumn = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
			int choice = (int) Math.floor(Math.random()*total);
			if (!cursor.moveToPosition(choice)) {
				return false;
			}
			currentArtist = cursor.getString(artistColumn);
			currentTitle = cursor.getString(titleColumn);
			dataUri = Uri.parse(cursor.getString(dataColumn));
			cursor.close();
		}

		buildNotification("Randomator", currentTitle, currentArtist);

		if (dataUri == null) {
			notifyError("Bad Uri");
			return false;
		}

		try {
			notifyChoice(currentTitle, currentArtist);
			mediaPlayer.setDataSource(getApplicationContext(), dataUri);
		} catch (IllegalStateException e) {
			return false;
		} catch (IOException e) {
			return false;
		}
		return true;
	}

	/**
	 * notify all attached listeners of an error
	 * @param s
	 */
	private void notifyError(String s) {
		for (Listener l: listeners) {
			l.onError(s);
		}
	}

	/**
	 * notify all attached listeners of a change in data source
	 * @param itemTitle
	 * @param itemArtist
	 */
	private void notifyChoice(String itemTitle, String itemArtist) {
		for (Listener l: listeners) {
			l.trackChanged(itemTitle, itemArtist);
		}
	}

	/*
	 * interface used to send information back to anyone binding to this service
	 */
	public interface Listener {
		void trackChanged(String title, String artist);
		void onError(String msg);
	}


	private ArrayList<Listener> listeners = new ArrayList<Listener>(); /* a collection of bound listeners */

	/**
	 * provide a simple local Binder for communication. assume that we will stay within the local process
	 */
	public class LocalBinder extends Binder {
		public void addListener(Listener l) {
			if (currentArtist != null && currentArtist != null) {
				l.trackChanged(currentTitle, currentArtist);
			}
			listeners.add(l);
		}
		public void removeListener(Listener l) {
			listeners.remove(l);
		}
	}

	protected LocalBinder binder = null;

	/**
	 * per track displayed text
	 * @param title
	 * @param artist
	 * @return
	 */
	protected String notificationString(String title, String artist) {
		if (title == null && artist == null) return "Playing";
		if (title == null || title.equals("")) return "Now playing "+artist;
		if (artist == null || artist.equals("")) return "Now playing "+'\"'+title+'\"';
		return  "Now playing " + '\"'+title+'\"' + " by "+artist;
	}

	/**
	 * builds a notification, which will display the current track and artist, and double as a stop button
	 * @param maintitle
	 * @param title
	 * @param artist
	 */
	protected void buildNotification(String maintitle, String title, String artist) {
		String text = notificationString(title, artist);
		if (maintitle == null) title = "Random Player";

		// setup what we will be building
		NotificationCompat.Builder builder = (NotificationCompat.Builder)
				new NotificationCompat.Builder(this)
						.setSmallIcon(android.R.drawable.ic_media_pause)
						.setContentTitle(maintitle)
						.setContentText(text);

		// create an intent for the notification
		Intent resultIntent = new Intent(ACTION_PAUSE);
		PendingIntent resultPendingIntent = PendingIntent.getService(this, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);
		builder.setContentIntent(resultPendingIntent);

		// kick it into action
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.notify(notificationId, builder.build());
	}

	/**
	 * clear notifications still running in our service
	 */
	protected void clearNotification() {
		NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(notificationId);
	}

}
