package com.mayaswell.randomplayer;

import android.Manifest;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.os.IBinder;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

/**
 * main activity ... used for starting and stopping the service, in addition to the widget ... provides slightly more
 * information, an interface to check permissions, and simplifies testing
 */
public class MainActivity extends AppCompatActivity {

	private ImageButton playButton;
	private ImageButton pauseButton;
	private TextView statusView;
	private TextView infoView;

	/**
	 * set up the interface
	 * @param savedInstanceState
	 */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		final MainActivity self = this;

		playButton = (ImageButton) findViewById(R.id.play_button);
		playButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				startPlayer(self);
			}
		});

		pauseButton = (ImageButton) findViewById(R.id.pause_button);
		pauseButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				stopPlayer(self);
			}
		});
		statusView = (TextView) findViewById(R.id.status_view);
		infoView = (TextView) findViewById(R.id.info_view);

	}

	/**
	 * do all the permission checking on startup, so avoiding any delay in onCreate()
	 */
	@Override
	protected void onStart() {
		super.onStart();
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			playButton.setVisibility(View.INVISIBLE);
			pauseButton.setVisibility(View.INVISIBLE);

			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
		}
	}

	/**
	 * set display appropriately according to what permissions we've been allowed
	 * @param requestCode
	 * @param permissions
	 * @param grantResults
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		boolean granted = true;
		for (int r: grantResults) {
			if (r != PackageManager.PERMISSION_GRANTED) {
				granted = false;
			}
		}
		if (granted) {
			playButton.setVisibility(View.VISIBLE);
			pauseButton.setVisibility(View.VISIBLE);
		} else {
			playButton.setVisibility(View.INVISIBLE);
			pauseButton.setVisibility(View.INVISIBLE);
			statusView.setText("NecessaryPermissions not granted");
		}
	}

	/**
	 * end of story
	 */
	@Override
	protected void onDestroy() {
		try {
			if (connection != null) unbindService(connection);
		} catch (IllegalArgumentException e) { // we're already stopped
			e.printStackTrace();
			Log.d("Main", e.getMessage());
		}
		super.onDestroy();
	}

	/**
	 * spawn our service
	 * @param context
	 */
	protected void startPlayer(Context context) {
		statusView.setText("");
		infoView.setText("");
		Log.d("main", "starting service");
		Intent intent = new Intent(context, RandomPlayer.class);
		intent.setAction(RandomPlayer.ACTION_PLAY);
		context.bindService(intent, connection, Context.BIND_AUTO_CREATE);
		context.startService(intent);

	}

	/**
	 * say goodbye to our service
	 * @param context
	 */
	protected void stopPlayer(Context context) {
		Log.d("main", "stopping service");
		infoView.setText("");
		statusView.setText("Stopping service");
		Intent intent = new Intent(context, RandomPlayer.class);
		intent.setAction(RandomPlayer.ACTION_PAUSE);
		try {
			context.unbindService(connection);
			context.stopService(intent);
		} catch (IllegalArgumentException e) { // we're already stopped
			e.printStackTrace();
			Log.d("Main", e.getMessage());
		}
	}

	/**
	 * binder for negotiating api with service
	 */
	private RandomPlayer.LocalBinder playerBinder = null;
	private RandomPlayer.Listener playerListener = new RandomPlayer.Listener() {
		@Override
		public void trackChanged(String title, String artist) {
			Log.d("MainActivity", "track changed to "+title+", "+artist);
			infoView.setText( "Listening to \""+title+"\" by "+artist);
		}

		@Override
		public void onError(String msg) {
			Log.d("MainActivity", "error "+msg);
			statusView.setText("Random service reports an error: "+msg);
		}
	};

	/**
	 * service connection to handle management of service link
	 */
	protected ServiceConnection connection = new ServiceConnection() {

		@Override
		public void onServiceConnected(ComponentName name, IBinder binder) {
			Log.d("MainActivity", "connected");
			try {
				playerBinder = (RandomPlayer.LocalBinder) binder;
			} catch (ClassCastException e) {
				return;
			}
			playerBinder.addListener(playerListener);
		}

		@Override
		public void onServiceDisconnected(ComponentName name) {
			Log.d("MainActivity", "disconnected");
			if (playerBinder != null) {
				infoView.setText("");
				statusView.setText("Stopped");
				playerBinder.removeListener(playerListener);
			}
		}
	};

}
