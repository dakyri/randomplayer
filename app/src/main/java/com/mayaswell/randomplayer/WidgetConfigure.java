package com.mayaswell.randomplayer;

import android.Manifest;
import android.app.Activity;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.TextView;

/**
 * Created by dak on 8/31/2016.
 *
 *  generic activity for handling widget configuration
 */
public class WidgetConfigure extends Activity {
	private int appWidgetId = AppWidgetManager.INVALID_APPWIDGET_ID;
	private TextView statusView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_configure);
		statusView = (TextView) findViewById(R.id.status_view);
		
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		if (extras != null) {
			appWidgetId = extras.getInt( AppWidgetManager.EXTRA_APPWIDGET_ID, AppWidgetManager.INVALID_APPWIDGET_ID);
			Log.d("WidgetConfig", "got widgid "+appWidgetId);
		} else {
			Log.d("WidgetConfig", "expected extras :( "+appWidgetId);
		}
	}

	/**
	 * do all the permission checking on startup, so avoiding any delay in onCreate()
	 */
	@Override
	protected void onStart() {
		super.onStart();
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
		} else {
			returnSuccess();
		}
	}
	/**
	 * setup widget appropriately according to what permissions we've been allowed
	 * @param requestCode
	 * @param permissions
	 * @param grantResults
	 */
	@Override
	public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
		boolean granted = true;
		for (int r: grantResults) {
			if (r != PackageManager.PERMISSION_GRANTED) {
				granted = false;
			}
		}
		if (granted) {
			returnSuccess();
		} else {
			statusView.setText("Widget not installed. Required permissions not available");
		}
	}

	private void returnSuccess() {
		Intent resultValue = new Intent();
		resultValue.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
		setResult(RESULT_OK, resultValue);
		finish();
	}

}